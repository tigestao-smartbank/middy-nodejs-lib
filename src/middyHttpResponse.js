const middyHttpResponse = (opts = {}) => {
    const httpResponseSerializerMiddlewareAfter = async (request) => {
        if (request.response === undefined) return
        request.response = returnSuccess(request)
    }

    return { after: httpResponseSerializerMiddlewareAfter }
}

function returnSuccess({ response, context, event }) {
    let result
    if (response.Items) {
        const prev = getPrev(event)
        const next = getNext(event)
        result = {
            meta: { page: { perPage: response.Count } },
            links: {
                prev: event.requestPath + '?offset=' + prev.offset + '&limit=' + prev.limit,
                next: event.requestPath + '?offset=' + next.offset + '&limit=' + next.limit
            },
            requestId: context.awsRequestId,
            data: response.Items
        }
    } else {
        result = { requestId: context.awsRequestId, data: response }
    }
    return result
}

function getPrev(event) {
    const offset = parseInt(event.query.offset)

    const prevLimit = parseInt(event.query.limit)
    const prev = { limit: prevLimit ? prevLimit : 30 }
    prev.offset = (offset - prev.limit) > 0 ? (offset - prev.limit) : 0

    return prev
}

function getNext(event) {
    let offset = parseInt(event.query.offset)
    offset = offset ? offset : 0

    const nextLimit = parseInt(event.query.limit)
    const next = { limit: nextLimit ? nextLimit : 30 }
    next.offset = offset + next.limit

    return next
}

module.exports = middyHttpResponse