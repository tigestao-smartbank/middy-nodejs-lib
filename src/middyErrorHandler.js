"use strict"
const { assign, omit } = require('lodash')

const defaults = { logger: console.error, fallbackMessage: null }
const hiddenFields = ['sqlState', 'errno', 'index', 'sql', 'expose', 'meta']

const handler = (opts = {}) => {
  const options = { ...defaults, ...opts }

  const httpErrorHandlerMiddlewareOnError = async request => {
    const { error } = request

    console.error(`An error occurred: ${error}`)
    if (typeof options.logger === 'function') { options.logger(error.message) }

    request.error = JSON.stringify(getErrorResponse(request))
  }
  return { onError: httpErrorHandlerMiddlewareOnError }
}


function getErrorResponse(request) {
  const { error } = request
  if (error.status) {
    return {
      requestId: request.context.awsRequestId,
      errors: [{
        status: error.status || 500,
        code: error.code || 'SYSTEM_ERROR',
        title: error.title || 'SystemError',
        detail: error.detail || options.fallbackMessage
      }]
    }
  }

  return assign(omit(error, hiddenFields), {
    requestId: request.context.awsRequestId,
    errors: [{
      status: error.response ? error.response.status : 500,
      code: error.response ? error.response.code : 'SYSTEM_ERROR',
      title: error.name || 'SystemError',
      detail: error.message || options.fallbackMessage
    }]
  })
}

const errorHandler = handler({
  logger: err => {
    console.error(err)
  }
})

module.exports = errorHandler