const inOutLog = require('@middy/input-output-logger')
const defaults = { logger: console.error, fallbackMessage: null }
const { isEmpty } = require('lodash')

const inOutLogger = inOutLog({
  awsContext: true,
  logger: ({ event, context, response }) => {
    if (!isEmpty(event)) {
      console.log('Event received', JSON.stringify(event))
      console.log('Context received', JSON.stringify(context))
    }
    
    if (!isEmpty(response))
      console.log('Lambda result', JSON.stringify(response))
  }
})

const errorHandler = (opts = {}) => {
  const options = { ...defaults, ...opts }

  const httpErrorHandlerMiddlewareOnError = async request => {
    const { error } = request
    console.error(`An error occurred: ${error}`)
    if (typeof options.logger === 'function') { options.logger(error.message) }
  }
  return { onError: httpErrorHandlerMiddlewareOnError }
}

const errorLogger = errorHandler({
  logger: err => {
    console.error(err)
  }
})

module.exports = { inOutLogger, errorLogger }