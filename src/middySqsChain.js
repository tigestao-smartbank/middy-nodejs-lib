const AWS = require('aws-sdk')
const _ = require('lodash')

const middySqsChain = {
  after: async ({ response }) => {
    if (response.QueueUrl && response.MessageBody) {
      const sqs = new AWS.SQS()
      if (_.isObject(response.MessageBody)) { response.MessageBody = JSON.stringify(response.MessageBody) }
      console.info("Sending to SQS: ", response)
      await sqs.sendMessage(response).promise()
    }
  }
}

module.exports = middySqsChain