# NodeJS Middy Library

Projeto que contem a biblioteca do Middy para uso em lambdas em NodeJS. O propósito dessa lib é para serem centralizadas as alterações que serão publicadas via lambda layer, para utilização nas lambdas, e para os testes unitários das lambdas, uma vez que não é possível testar o funcionamento real de layers, fazendo com que não seja necessário a criação de mocks para o uso do 'letsbank-middy'

## Preparando o ambiente
### Instalações
É preciso ter instalado em sua máquina as seguintes tecnologias para poder atuar no projeto:
* [npm](https://www.npmjs.com/get-npm)
* [VS Code](https://code.visualstudio.com/Download) como IDE sugerida

## Utilização
### Lambda Layer
Para a utilização dessa lib em lambdas como layers, verificar o projeto [middy-nodejs-layer](https://bitbucket.org/tigestao-smartbank/middy-nodejs-layer/)
Em seu projeto cliente de uma das layers publicadas por este projeto, considerando que este também é um projeto que utiliza o Serverless Framework, basta fazer uma configuração como esta:

```yml
# ...

functions:
  MyFunction:
    name: ${opt:env}-${self:service}-myFunction
    handler: src/functions/myFunction.handle
    layers:
      - ${cf:middy-layer-${opt:env}.MiddyLambdaLayerQualifiedArn}

# ....
```
### Lib para testes unitários
No seu projeto, criado em [Serverless Framework](https://www.serverless.com/framework/docs/), declarar como devDependency em seu package.json essa lib, bem como as dependencias do middy e lodash, da seguinte forma:

```json
  "devDependencies": {
    "letsbank-middy": "git+ssh://git@bitbucket.org:tigestao-smartbank/middy-nodejs-lib.git",
    "@middy/core": "^2.5.3",
    "@middy/error-logger": "^2.5.3",
    "@middy/http-cors": "^2.5.3",
    "@middy/http-event-normalizer": "^2.5.3",
    "@middy/http-json-body-parser": "^2.5.3",
    "@middy/http-response-serializer": "^2.5.3",
    "@middy/input-output-logger": "^2.5.3",
    "@middy/sqs-json-body-parser": "^2.5.3",
    "@middy/validator": "^2.5.3",
    "lodash": "^4.17.21",
    "mocha": "^9.1.3",
    "proxyquire": "^2.1.3"
  }
```

Dessa forma, não é necessário mais nenhuma adequação ou mock para o comportamento do middy, pois o seu projeto já irá conter a lib para os testes.
#### Importante
Para utilização da lib para execução de testes unitários, primeiramente é necessário que o repositório do seu projeto tenha permissão para acessar este repositório. Para solicitar permissão, é necessário criar uma tarefa para o time de DevOps, no quadro [TDDO - Jira](https://tigestao-smartbank.atlassian.net/jira/software/c/projects/TDDO/boards/160)