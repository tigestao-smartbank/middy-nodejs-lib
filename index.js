const middy = require('@middy/core')
const sqsParser = require('@middy/sqs-json-body-parser')
const validator = require('@middy/validator')
const normalizer = require('@middy/http-event-normalizer')
const errorHandler = require('./src/middyErrorHandler')
const sqsBatchSizeOne = require('./src/middySqsBatchSizeOne')
const sqsChain = require('./src/middySqsChain')
const cors = require('@middy/http-cors')
const { inOutLogger, errorLogger } = require('./src/middyLogger')
const middyHttpResponse = require('./src/middyHttpResponse')

const httpMiddlewares = [normalizer(), cors(), middyHttpResponse()]
const sqsMiddlewares = [sqsParser()]
const functionMiddlewares = [inOutLogger, errorHandler]

function event(eventProcessor, middlewares = []) {
  return middy(eventProcessor)
    .use(functionMiddlewares)
    .use(middlewares)
}

function sqsEvent(eventProcessor, middlewares = []) {
  return middy(eventProcessor)
    .use(inOutLogger)
    .use(errorLogger)
    .use(sqsMiddlewares)
    .use(middlewares)
}

function stepFunctionsEvent(eventProcessor, middlewares = []) {
  return middy(eventProcessor)
    .use(inOutLogger)
    .use(errorLogger)
    .use(middlewares)
}

function httpEvent(eventProcessor, inputSchema = {}, middlewares = []) {
  return middy(eventProcessor)
    .use(functionMiddlewares)
    .use(httpMiddlewares)
    .use(validator({ inputSchema, defaultLanguage: 'pt-BR' }))
    .use(middlewares)
}

module.exports = { httpEvent, event, sqsEvent, stepFunctionsEvent, sqsBatchSizeOne, sqsChain }